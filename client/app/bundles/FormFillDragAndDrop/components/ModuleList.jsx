import ModuleContainer from '../containers/ModuleContainer'


// This implements the list of available modules that can be added to the form
class ModuleList extends React.Component {

    render() {
        var moduleNodes = this.props.modules.map(function(element, index) {
            return (<ModuleContainer key={element.id} index={index} module={element} canDrag={true} canDrop={false}/>)
        });
        return (
            <div className="moduleList list-group">
                {moduleNodes}
            </div>
        )
    }
}

module.exports = ModuleList;
