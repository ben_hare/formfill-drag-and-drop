import React, { PropTypes } from 'react';
import _ from 'lodash';
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';  // For ReactDnD, can be replaced with any backend

import FormFillStore from '../../../stores/FormFillStore';
import FormFillActions from '../../../actions/FormFillActions';

import OrderContainer from '../containers/OrderContainer'
import ModuleContainer from '../containers/ModuleContainer'
import OrderListContainer from '../containers/OrderListContainer'
import ModuleListContainer from '../containers/ModuleListContainer'
import DocumentContainer from '../containers/DocumentContainer'


// Overall Page layout
// This is an alt Container View which hooks into the alt setup
class FormFillPage extends React.Component {
    constructor(props, context) {
        super(props, context);
        // Uses lodash to bind all methods to the context of the object instance, otherwise
        // the methods defined here would not refer to the component's class, not the component
        // instance itself.
        _.bindAll(this, '_handleChange');
    }

    state = FormFillStore.getState();

    componentDidMount() {
        FormFillStore.listen(this._handleChange);

        FormFillActions.fetchElements();
    };

    componentWillUnmount() {
        FormFillStore.unlisten(this._handleChange);
    };

    _handleChange(state) {
        this.setState(state);
    };

    render() {
        var orders = [];  // Separate out the orders and modules from the list. Implement this when real JSON data is used
        var modules = [];
        for (var i = 0; i < this.state.elements.length; i++) {
            var element = this.state.elements[i];
            if (element.type === 'order') {
                orders.push(element)
            }
            else if (element.type === 'module') {
                modules.push(element)
            }
        }
        return (
            <div className="FormFillPage container">
                <div className='col-md-6'>
                    <ModuleListContainer modules={modules} />
                    <OrderListContainer orders={orders} />
                </div>
                <div className='col-md-6'>
                    <DocumentContainer />
                </div>
            </div>
        );
    }
}

module.exports = DragDropContext(HTML5Backend)(FormFillPage);