import OrderContainer from '../containers/OrderContainer'


// This implements the base set of orders that can be dragged into modules on the form
class OrderList extends React.Component {

    render() {
        var orderNodes = this.props.orders.map(function(element) {
            return (<OrderContainer key={element.id} order={element} canDrag={true} />)
        });
        return (
            <div className="orderList list-group">
                {orderNodes}
            </div>
        )
    }
}

module.exports = OrderList;