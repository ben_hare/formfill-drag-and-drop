import OrderContainer from '../containers/OrderContainer'
import ModuleContainer from '../containers/ModuleContainer'


// This implements a Module, and sets the interior elements to be non-dragable
class Module extends React.Component {

    render() {
        var canDrop = this.props.canDrop;
        var elementNodes = this.props.module.elements.map(function(element, index) {
            if (element.type === 'order') {
                return (<OrderContainer order={element} index={index} canDrag={false}/>)
            }
            else if (element.type === 'module') {
                return (<ModuleContainer module={element} index={index} canDrag={false} canDrop={canDrop}/>)
            }
        });
        return (
            <div className="module list-group-item" id={this.props.module.id}>
                {this.props.module.text_representation}
                {elementNodes}
            </div>
        )
    }
}

module.exports = Module;