import DocumentContainer from '../containers/DocumentContainer'
import OrderContainer from '../containers/OrderContainer'
import ModuleContainer from '../containers/ModuleContainer'


// This is the form fill document that can have modules and orders added to it, but these are added to the container
class Document extends React.Component {

    render() {
        var elementNodes = this.props.elements.map(function(element, index) {
            if (element.type === 'order') {
                return (<OrderContainer order={element} index={index} canDrag={false} />)
            }
            else if (element.type === 'module') {
                return (<ModuleContainer module={element} index={index} canDrag={false} canDrop={true}/>)
            }
        });
        return (
            <div className="Document" style={{
                minHeight: '400px'
            }}>
                {elementNodes}
            </div>
        )
    }
}

module.exports = Document;