
var Order = ({order}) => (
    <div className="order list-group-item">
        <div id={order.id}>
            {order.text_representation}
        </div>
    </div>

);

module.exports = Order;