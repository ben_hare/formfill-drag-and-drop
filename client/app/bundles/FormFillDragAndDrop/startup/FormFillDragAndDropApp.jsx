import React from 'react';
import FormFillPageContainer from '../containers/FormFillPageContainer';

const FormFillDragAndDropApp = props => {
  const reactComponent = (
    <FormFillPageContainer {...props} />
  );
  return reactComponent;
};

export default FormFillDragAndDropApp;
