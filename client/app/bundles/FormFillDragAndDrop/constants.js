
// Store the various allowed drag and drop types here (so that you exclude different objects from
// different drag and drop sources/targets)
exports.ItemTypes = {
    ORDER: 'order',
    MODULE: 'module'
};