import ModuleDropContainer from './ModuleDropContainer'
import { DragSource } from 'react-dnd';
import { ItemTypes } from '../constants';
import { PropTypes } from 'react';


// Defines both what is passed when dragged and whether it is allowed to be dragged at all
const moduleSource = {
    beginDrag(props) {
        return {element: props.module};
    },
    canDrag(props) {
        return props.canDrag
    }
};

// This injects properties into the props that are passed into ModuleContainer
function collect(connect, monitor) {
    return {
        connectDragSource: connect.dragSource(),
        isDragging: monitor.isDragging()  // Can be used to overlay something as the module is being moved
    }
}

// This is the dragable container for the module
class ModuleContainer extends React.Component {
    constructor() {
        super();
    }

    static propTypes = {
        connectDragSource: PropTypes.func.isRequired,
        isDragging: PropTypes.bool.isRequired
    };

    render() {
        var connectDragSource = this.props.connectDragSource;
        return connectDragSource(<div><ModuleDropContainer module={this.props.module} index={this.props.index} canDrag={this.props.canDrag} canDrop={this.props.canDrop}/></div>);
    }
}

export default DragSource(ItemTypes.MODULE, moduleSource, collect)(ModuleContainer)