import ModuleList from '../components/ModuleList'


// Container class for the module list
class ModuleListContainer extends React.Component {
    constructor() {
        super();
    }

    render() {
        return (<ModuleList modules={this.props.modules} />);
    }
}

module.exports = ModuleListContainer;