import React, { PropTypes } from 'react';
import FormFillPage from '../components/FormFillPage';
import _ from 'lodash';

// Simple example of a React "smart" component
export default class FormFillPageContainer extends React.Component {
  constructor(props, context) {
    super(props, context);

    // Uses lodash to bind all methods to the context of the object instance, otherwise
    // the methods defined here would not refer to the component's class, not the component
    // instance itself.
  }

  static propTypes = {};

  state = {}; // how to set initial state in es2015 class syntax


  render() {
    return (
      <div>
        <FormFillPage />
      </div>
    );
  }
}
