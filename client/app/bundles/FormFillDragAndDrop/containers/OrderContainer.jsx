import { DragSource } from 'react-dnd';
import OrderDropContainer from '../containers/OrderDropContainer'
import { ItemTypes } from '../constants';
import { PropTypes } from 'react';


// This controls what is passed each time an order is dragged, and whether it can be dragged at all
const orderSource = {
    beginDrag(props) {
        return {element: props.order};
    },
    canDrag(props) {
        return props.canDrag
    }
};

// This injects properties into the props of the dragged order container
function collect(connect, monitor) {
    return {
        connectDragSource: connect.dragSource(),
        isDragging: monitor.isDragging()
    }
}

// This is the container for orders
class OrderContainer extends React.Component {
    constructor() {
        super();
    }

    static propTypes = {
        connectDragSource: PropTypes.func.isRequired,
        isDragging: PropTypes.bool.isRequired
    };

    render() {
        var connectDragSource = this.props.connectDragSource;
        return connectDragSource(<div><OrderDropContainer order={this.props.order} index={this.props.index} /></div>);
    }
}

export default DragSource(ItemTypes.ORDER, orderSource, collect)(OrderContainer)