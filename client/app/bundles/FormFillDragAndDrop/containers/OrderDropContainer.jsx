import Order from '../components/Order'
import { ItemTypes } from '../constants';
import { DropTarget } from 'react-dnd';
import ModuleUpdateActions from '../../../actions/ModuleUpdateActions';


// This injects properties into the props of the drop container
function collect(connect, monitor) {
    return {
        connectDropTarget: connect.dropTarget(),
        isOver: monitor.isOver({shallow: true})
    };
}

// This specifies what to do with the data that is received when something is dropped in, and also
// whether anything can be dropped into it at all
const orderDropTarget = {
    drop(props, monitor, component) {
        return false
    },
    canDrop(props) {
        // Only exists to get at the isOver property, not actually droppable
        return false
    }
};

// Need an order drop container as well so it can let indicate when the dragged object is over it
class OrderDropContainer extends React.Component {
    constructor() {
        super();
    }

    render() {
        var isOver = this.props.isOver;
        if (isOver) {
            ModuleUpdateActions.updateOverElementIndex(this.props.index)
        }
        var connectDropTarget = this.props.connectDropTarget;
        return connectDropTarget(<div><Order order={this.props.order} /></div>);
    }
}

module.exports = DropTarget(ItemTypes.ORDER, orderDropTarget, collect)(OrderDropContainer);
