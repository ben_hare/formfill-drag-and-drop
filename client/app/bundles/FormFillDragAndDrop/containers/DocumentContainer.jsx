import Document from '../components/Document'
//import FormFillActions from '../../../actions/FormFillActions';
import { ItemTypes } from '../constants';
import { DropTarget } from 'react-dnd';
import ModuleUpdateStore from '../../../stores/ModuleUpdateStore';


function collect(connect, monitor) {
    return {
        connectDropTarget: connect.dropTarget(),
        isOver: monitor.isOver({shallow: true})
    };
}

const documentTarget = {
    drop(props, monitor, component) {
        if (!monitor.didDrop()) {  // If a higher element has already seen this event, don't actually add it
            var module = monitor.getItem().element;
            var newModule = (JSON.parse(JSON.stringify(module)));
            component.state.elements.splice(ModuleUpdateStore.getState().overElementIndex+1, 0, newModule);
            //FormFillActions.removeElement(module);  // Uncomment this to allow modules to be removed on addition
            component.forceUpdate();
        }
    }
};

class DocumentContainer extends React.Component {
    constructor() {
        super();
    }

    state = {elements: []};

    render() {
        var isOver = this.props.isOver;
        var connectDropTarget = this.props.connectDropTarget;
        return connectDropTarget(<div className='documentContainer well' style={{
                zIndex: isOver ? '1' : '',
                backgroundColor: isOver ? 'aliceblue' : ''
            }}>
            <Document elements={this.state.elements} isOver={this.props.isOver}/>
        </div>);
    }
}

module.exports = DropTarget(ItemTypes.MODULE, documentTarget, collect)(DocumentContainer);