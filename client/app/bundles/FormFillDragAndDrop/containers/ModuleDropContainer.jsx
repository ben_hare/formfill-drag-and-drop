import Module from '../components/Module'
import { ItemTypes } from '../constants';
import { DropTarget } from 'react-dnd';
import _ from 'lodash';

import ModuleUpdateStore from '../../../stores/ModuleUpdateStore';


// This injects properties into the props of the drop container
function collect(connect, monitor) {
    return {
        connectDropTarget: connect.dropTarget(),
        isOver: monitor.isOver({shallow: true})
    };
}

// This specifies what to do with the data that is received when something is dropped in, and also
// whether anything can be dropped into it at all
const moduleDropTarget = {
    drop(props, monitor, component) {
        if (!monitor.didDrop()) {  // Only add the component if it's the highest element that it was hovered over
            console.log(ModuleUpdateStore.getState());
            var element = monitor.getItem().element;
            //console.log(document.elementFromPoint(event.clientX, event.clientY));
            var newElement = (JSON.parse(JSON.stringify(element)));
            component.props.module.elements.splice(ModuleUpdateStore.getState().overElementIndex+1, 0, newElement);
            component.forceUpdate();
        }
    },
    canDrop(props) {
        // If a module is dragable, it can't have things dropped into it.
        return props.canDrop
    }
};

// Since modules are both dragable and dropable, there's a sub container underneath the basic container
// that can in fact have elements dropped into it
class ModuleDropContainer extends React.Component {
    constructor() {
        super();
    }

    render() {
        var isOver = this.props.isOver;
        var connectDropTarget = this.props.connectDropTarget;
        return connectDropTarget(<div style={{
            opacity: (isOver && this.props.canDrop) ? '0.5' : ''
        }}>
            <Module module={this.props.module} canDrop={this.props.canDrop}/>
        </div>);
    }
}

module.exports = DropTarget([ItemTypes.ORDER, ItemTypes.MODULE], moduleDropTarget, collect)(ModuleDropContainer);
