import OrderList from '../components/OrderList'


// The container class for the order list
class OrderListContainer extends React.Component {
    constructor() {
        super();
    }

    render() {
        return (<OrderList orders={this.props.orders} />);
    }
}

module.exports = OrderListContainer;