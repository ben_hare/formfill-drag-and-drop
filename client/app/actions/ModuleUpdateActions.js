import alt from '../alt';


class ModuleUpdateActions {
    updateOverElementIndex(index) {
        this.dispatch(index)
    }
}

module.exports = alt.createActions(ModuleUpdateActions);
