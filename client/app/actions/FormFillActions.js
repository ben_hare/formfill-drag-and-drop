import alt from '../alt';
import FormFillSource from '../sources/FormFillSource'


class FormFillActions {
    updateElements(elements) {
        this.dispatch(elements)
    }
    removeElement(element) {
        this.dispatch(element)
    }
    fetchElements() {
        this.dispatch();
        FormFillSource.fetch()
            .then((elements) => {
                this.actions.updateElements(elements)
            })
            .catch((errorMessage) => {
                // Fail silently for now
                console.log(errorMessage)
            })
    }
}

module.exports = alt.createActions(FormFillActions);