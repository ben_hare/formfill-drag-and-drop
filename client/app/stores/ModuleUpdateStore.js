import alt from '../alt';
import ModuleUpdateActions from '../actions/ModuleUpdateActions';


class ModuleUpdateStore {
    constructor() {
        this.overElementIndex = null;

        this.bindListeners({
            updateOverElementIndex: ModuleUpdateActions.UPDATE_OVER_ELEMENT_INDEX
        });
    }

    updateOverElementIndex(index) {
        this.overElementIndex = index;
    }
}

module.exports = alt.createStore(ModuleUpdateStore, 'ModuleUpdateStore');

