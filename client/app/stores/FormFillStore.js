import alt from '../alt';
import FormFillActions from '../actions/FormFillActions';


class FormFillStore {
    constructor() {
        this.elements = [];

        this.bindListeners({
            handleUpdateElements: FormFillActions.UPDATE_ELEMENTS,
            handleFetchElements: FormFillActions.FETCH_ELEMENTS,
            handleRemoveElement: FormFillActions.REMOVE_ELEMENT
        });
    }

    handleUpdateElements(elements) {
        this.elements = elements;
    }

    handleFetchElements(elements) {
        this.elements = [];
    }

    handleRemoveElement(element) {
        var index = this.elements.indexOf(element);
        if (index > -1) {
            this.elements.splice(index, 1);
        }
    }
}

module.exports = alt.createStore(FormFillStore, 'FormFillStore');