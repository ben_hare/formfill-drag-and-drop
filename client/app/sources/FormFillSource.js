

var mockElementData = [
    {
        'id': 'm1',
        'type': 'module',
        'text_representation': 'First Module',
        'elements': [
            {
                'id': 'o1',
                'type': 'order',
                'text_representation': 'First Order'
            },
            {
                'id': 'o2',
                'type': 'order',
                'text_representation': 'Second Order'
            },
            {
                'type': 'module',
                'text_representation': 'Fourth Module',
                'elements': [
                    {
                        'id': 'o10',
                        'type': 'order',
                        'text_representation': 'Tenth Order'
                    },
                    {
                        'id': 'o11',
                        'type': 'order',
                        'text_representation': 'Eleventh Order'
                    }
                ]
            }
        ]
    },
    {
        'id': 'm2',
        'type': 'module',
        'text_representation': 'Second Module',
        'elements': [
            {
                'id': 'o3',
                'type': 'order',
                'text_representation': 'Third Order'
            },
            {
                'id': 'o4',
                'type': 'order',
                'text_representation': 'Fourth Order'
            }
        ]
    },
    {
        'id': 'm3',
        'type': 'module',
        'text_representation': 'Third Module',
        'elements': [
            {
                'id': 'o5',
                'type': 'order',
                'text_representation': 'Fifth Order'
            },
            {
                'id': 'o6',
                'type': 'order',
                'text_representation': 'Sixth Order'
            }
        ]
    },
    {
        'id': 'o7',
        'type': 'order',
        'text_representation': 'Seventh Order'
    },
    {
        'id': 'o8',
        'type': 'order',
        'text_representation': 'Eighth Order'
    },
    {
        'id': 'o9',
        'type': 'order',
        'text_representation': 'Ninth Order'
    },
    {
        'id': 'o12',
        'type': 'order',
        'text_representation': 'Twelfth Order'
    },
    {
        'id': 'o13',
        'type': 'order',
        'text_representation': 'Thirteenth Order'
    },
    {
        'id': 'o14',
        'type': 'order',
        'text_representation': 'Fourteenth Order'
    }
];


var FormFillSource = {
    // TODO Use an actual API call for the data here eventually
    fetch: function () {
        return new Promise(function (resolve, reject) {
           setTimeout(function () {
               resolve(mockElementData);
           }, 100);
        });
    }
};

module.exports = FormFillSource;