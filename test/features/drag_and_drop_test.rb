require "test_helper"
require 'selenium-webdriver'

feature "DragAndDrop" do

  background do
    visit '/form_fill_drag_and_drop'
  end

  scenario "the page initializes", js: true do
    current_path.must_equal '/form_fill_drag_and_drop'
    page.must_have_content 'Form Fill Drag And Drop'
    page.must_have_content 'First Module'
  end

  scenario "module dragged into document is added to document", js: true do
    module_to_drag = page.find('#m1')
    document_to_drop = page.find('.well')
    module_to_drag.drag_to(document_to_drop)
    save_and_open_page

    page.find('.Document').must_have_content 'First Module'
  end

  scenario "order dragged into document is not added to document", js: true do
    order_to_drag = page.find('#o7')
    document_to_drop = page.find('.well')
    order_to_drag.drag_to(document_to_drop)

    page.find('.Document').wont_have_content 'Seventh Order'
  end

  scenario "order dragged into module on document is added to module", js: true do
    module_to_drag_and_drop = page.find('#m1')
    document_to_drop = page.find('.well')
    module_to_drag_and_drop.drag_to(document_to_drop)
    order_to_drag = page.find('#o7')
    order_to_drag.drag_to(module_to_drag_and_drop)

    page.find('.Document').must_have_content 'Seventh Order'
  end


end